const nanorepServer = 'team-cia.nanorep.info';
const nanorepAccount = 'aurorahack';
const knowledgeBase = 'aurora1';
const nanorepApiKey = '24ff53b5-53af-4f88-8961-9a1f22aac7d1';
let nanorepWidgetInitialized = false;

const observer = new MutationObserver(function(mutations, observer) {
	const alert = document.querySelector('.questions .alert-info');
	const formElements = document.querySelectorAll('.questions .form-group.col-sm-12');
	const submitButton = document.querySelector('.sectionFooter button');

	if(alert) {
		alert.remove();
	}
	if(formElements.length > 1) {
		formElements[0].remove();
	}
	/*
	// send question to nanorep manually
	if(submitButton && !submitButton.onclick) {
		submitButton.onclick = function () {
			fetch(`https://team-cia.nanorep.info/~aurorahack/api/kb/v1/simpleSearch?account=${nanorepAccount}&kb=${knowledgeBase}&apikey=${nanorepApiKey}&query=${encodeURIComponent(document.querySelector('.questions .form-group.col-sm-12 input').value)}`)
		};
	}
	*/
	// set up nanorep widget
	if(formElements.length === 1) { // question is the only form element
		initNanorepWidget(formElements);
	}
});

observer.observe(document, {
	subtree: true,
	attributes: true
});

function initNanorepWidget(formElements) {
	if (nanorepWidgetInitialized) {
		return;
	}

	const questionInput = formElements[0].querySelector('input');
	questionInput.insertAdjacentHTML('afterend', '<div id="nanorep-embedded-widget"></div>');
	questionInput.remove();

	!function(t,e,o,c,n,a){var s=window.nanorep=window.nanorep||{};s=s[e]=s[e]||{},s.apiHost=a,s.host=n,s.path=c,s.account=t,s.protocol="https:",s.on=s.on||function(){s._calls=s._calls||[],s._calls.push([].slice.call(arguments))};var p=s.protocol+"//"+n+c+o+"?account="+t,l=document.createElement("script");l.async=l.defer=!0,l.setAttribute("src",p),document.getElementsByTagName("head")[0].appendChild(l)}(nanorepAccount,"embeddedWidget","embedded-widget.js","/web/",nanorepServer);

	nanorep.embeddedWidget.on({
		init: function() {
			//this.setConfigId('904201972');
			this.setKB(knowledgeBase);
			this.setContext({"scenario":"pre"});
		}
	});

	nanorepWidgetInitialized = true;
}

