let submitButton;
let questionInputTextArea;
let questionInputTextAreaValue = '';

const nanorepAccount = 'aurorahack';
const knowledgeBase = 'aurora1';
const nanorepApiKey = '24ff53b5-53af-4f88-8961-9a1f22aac7d1';

const allQuestions = [];

MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

checkForAnswers();

const observer = new MutationObserver(function(mutations, observer) {
	submitButton = document.querySelector('.send-button-question');
	questionInputTextArea = document.querySelector('.qanda-send textarea');

	const questionsButton = document.querySelector('.nav__primary #qanda-link');
	if(questionsButton && !questionsButton.onclick) {
		questionsButton.onclick = function () {
			setTimeout(function() {
				const unstyledList = document.querySelectorAll('.sidebar-panel .list-unstyled')[0];
				if(unstyledList) {
					unstyledList.innerHTML = '';
					unstyledList.style.display = 'block';
					loadFAQ().then(response => {
						response.articles.forEach(article => {
							unstyledList.insertAdjacentHTML('beforeend', addFAQuestion(article.title, article.body));
							const faqItem = document.querySelector(`li[data-question="${article.title}"]`);
							console.log('AAAA: ', faqItem);
							faqItem.onclick = function() {
								const wrapper= document.createElement('div');
								wrapper.innerHTML= addListElement(article.title, article.body);
								const expandedElement = wrapper.firstChild;
								faqItem.parentNode.replaceChild(expandedElement, faqItem);
								expandedElement.onclick = function() {
									expandedElement.parentNode.replaceChild(faqItem, expandedElement);
								}
							}
						});
					});
				}
			}, 100);
		};

		setTimeout(() => {
			questionsButton.click();
		}, 2000);
	}

	if(submitButton && !submitButton.onclick) {
		submitButton.onclick = function () {
			sendQuestion();
		};
	}

	if(questionInputTextArea) {
		questionInputTextArea.addEventListener('input', function(){
			questionInputTextAreaValue = questionInputTextArea.value;
		});
	}
});

observer.observe(document, {
	subtree: true,
	attributes: true
});

function sendQuestion() {
	console.log('Question to send: ', questionInputTextAreaValue);
	allQuestions.push(questionInputTextAreaValue);
	fetch(`https://team-cia.nanorep.info/~aurorahack/api/kb/v1/simpleSearch?account=${nanorepAccount}&kb=${knowledgeBase}&apikey=${nanorepApiKey}&query=${encodeURIComponent(questionInputTextAreaValue)}&context=scenario:meeting`)
		.then(response => {
			return response.json();
		})
		.then(data => {
			if(data.data.length) {
				const allElements = document.querySelectorAll('.sidebar-panel .list-unstyled li');
				const lastElement = allElements[allElements.length - 1];
				lastElement.style['background-color'] = '#e5f1e3';
				const lastQuestion = lastElement.querySelector('dd');
				lastQuestion.insertAdjacentHTML('afterend', `<div class="qanda-answer"><dt class="pull-left">A:</dt><dd data-bind="text: answerText">${data.data[0].body}</dd></div>`);
				const answeredByList = document.querySelectorAll('.sidebar-panel .list-unstyled li .chat-sender-colored');
				answeredByList[answeredByList.length - 1].innerHTML = "Answered by BOT";
			}
		});
}

function checkForAnswers() {
	setInterval(() => {
		const allQuestions = document.querySelectorAll('.sidebar-panel .list-unstyled li');
		createConversation().then(conversationId => {
			for(let i = 0; i < allQuestions.length; i++) {
				if(!allQuestions[i].querySelector('.qanda-answer') && !allQuestions[i].getAttribute('data-answer')) {
					const listElement = allQuestions[i].querySelector('.sidebar-panel .list-unstyled li dd');
					if(listElement) {
						sendConversationStatement(conversationId, listElement.textContent).then(answerText => {
							if(answerText) {
								allQuestions[i].style['background-color'] = '#e5f1e3';
								const itemToInsertAnswer = allQuestions[i].querySelector('dd');
								itemToInsertAnswer.insertAdjacentHTML('afterend', `<div class="qanda-answer"><dt class="pull-left">A:</dt><dd data-bind="text: answerText">${answerText}</dd></div>`);
							}
						});
					}
				}
			}
		});;



	}, 5000);
}

function createConversation() {
	return fetch(`https://team-cia.nanorep.info/~aurorahack/api/conversation/v2/create?account=${nanorepAccount}&kb=${knowledgeBase}&apikey=${nanorepApiKey}&disableStatistics=true&context=scenario:meeting`)
		.then(response => {
			return response.json();
		}).then(conversation => {
			return conversation.id;
		});
}

function sendConversationStatement(conversationId, question) {
	console.log('Sending statement: ', question);
	return fetch(`https://team-cia.nanorep.info/~aurorahack/api/conversation/v1/statement?account=${nanorepAccount}&id=${conversationId}&statement=${encodeURIComponent(question)}`)
		.then(response => {
			return response.json();
		}).then(statementAnswer => {
			if(statementAnswer.articleId !== '0') {
				return statementAnswer.text;
			} else {
				return null;
			}
		});
}

function loadFAQ() {
	return fetch(`https://team-cia.nanorep.info/api/kb/v1/export?apiKey=${nanorepApiKey}&account=${nanorepAccount}&kb=${knowledgeBase}&format=json&maxItems=5&skip=0&bypopularity=true`)
		.then(response => {
			console.log('RESPONSE: ', response);
			return response.json();
		}).then(articles => {
			console.log('Articles: ', articles);
			return articles;
		});
}

function addListElement(question, answer) {
	return `<li class="question-item" data-bind="css: {
                answered: answered,
                public: $data.hasOwnProperty('isPublicAnswer') &amp;&amp; isPublicAnswer === true,
                private: $data.hasOwnProperty('isPublicAnswer') &amp;&amp; isPublicAnswer === false
            }" style="background-color:#F3F3F3;cursor:pointer">

                <div class="question-header clearfix" style="color:#666">
                    <span class="chat-sender"><span class="chat-sender-colored">FAQ Article</span></span>
                </div>


                <dl>
                    <dt class="pull-left qanda-question">Q:</dt>
                    <dd class="qanda-question" data-bind="text: questionText" style="color:#333">
                    	${question}
					</dd>
					<div class="qanda-answer">
					<dt class="pull-left">A:</dt>
					<dd data-bind="text: answerText">
						${answer}
					</dd>
					</div>

                    <div class="chat-status">
                        <span></span>
                    </div>
                </dl>

            </li>`
}

function addFAQuestion(question, answer) {
	return `<li class="question-item" data-bind="css: {
                answered: answered,
                public: $data.hasOwnProperty('isPublicAnswer') &amp;&amp; isPublicAnswer === true,
                private: $data.hasOwnProperty('isPublicAnswer') &amp;&amp; isPublicAnswer === false
            }" style="background-color:#F3F3F3;padding:3px;cursor:pointer;margin-bottom:1px" data-answer="${answer}" data-question="${question}">
                <dl>
                    <dd class="qanda-question" data-bind="text: questionText" style="color:#333">
                    	${question}
					</dd>
                </dl>
            </li>`
}


